// mon premier commentaire
/*
un paragraphe de commentaire
sur plusieurs lignes
*/
console.log("mon premier script javascript")

let nom = "kevin" //string

let age = 25 //number

let possedeLePermi = true //boolean

let notes = [34, 36, 38, 33, 39] //array

let moyenneParMatiere = {
    "français": 15,
    "mathématiques": 13,
    "histoire/géo": 16
} //object

let objetComplexe = {
    "clé": "une chaine de caractère",
    "clé2": [15, 12, 16, 13],
    "clé3": {
        "clé11": 15,
        "clé12": ["texte1","text2"]
    }
} //object plus complexe

console.log(nom, age, possedeLePermi)

if(age < 18) {
    console.log("je suis mineur")
} else if(age > 65 ) {
    console.log('je suis majeur et à la retraite')
} else {
    console.log("je suis majeur")
}

for(let i=0; i<10; i++){
    console.log(i)
}

let somme = 0
for(let note of notes){
    somme = somme + note
}

console.log(somme/notes.length)
console.log(notes)
console.log(moyenneParMatiere)

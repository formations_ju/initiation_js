---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
logoImg: "javascript.png"
slideNumber: false
title: "VSCode Reveal intro"
---

## Initiation au Javascript

<small>proposé par Julien Graziani</small>

---

### Qu'est-ce que c'est que le Javascript ? 

<span class="fragment">Ce n'est pas un script Java.</span>

<span class="fragment">C'est un langage de programmation indépendant.</span>

---

### Ou est-ce qu'on s'en sert ? 

On peut s'en servir de partout ...

mais

... on va s'en servir dans le navigateur.

---

### Qu'est qu'on fait dans le navigateur ?

Modification du DOM

Appels aux serveurs

de l'algorithmie

...

---

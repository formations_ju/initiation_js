console.log("mon deuxième script javascript")

let main = document.getElementById("main")

let paragraphe = document.getElementById("paragraphe")

paragraphe.addEventListener("click", () => {
    paragraphe.style.color = "#0000FF"
})

paragraphe.addEventListener("mouseover", () => {
    paragraphe.style.color = "#00FF00"
})

paragraphe.addEventListener("mouseout", () => {
    paragraphe.style.color = "#FF0000"
})

let newParagraphe = document.createElement("p")
newParagraphe.innerText = "mon lorem ipsum customisé"


let result = document.getElementById("result")

result.insertAdjacentElement("afterbegin", newParagraphe)
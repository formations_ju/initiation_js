console.log("mon troisième script javascript")

let result = document.getElementById("result")

let appelAPI = async () => {
    let response = await fetch("https://reqres.in/api/unknown", {method:'GET',})
    let data = await response.json()

    console.log(data)
    let colors = data.data

    let ul = document.getElementById("colors");
    for(let i=0; i<colors.length; i++) {
        let li = document.createElement("li")
        li.innerText = colors[i].name
        ul.insertAdjacentElement("beforeend",li)

        li.addEventListener("mouseover", event => {
            for(let color of colors) {
                if(color.name === event.target.innerText) {
                    result.innerText = JSON.stringify(color)
                    result.style.color = color.color
                }
            }
        })

    }
}

let appelAPIPlaceholderPhotos = async () => {
    let response = await fetch("https://jsonplaceholder.typicode.com/photos", {method:'GET',})
    let photos = await response.json()

    console.log(photos)

    let balisePhoto = document.getElementById("photos")

    for(let i=0; i<20; i++) {
        let li = document.createElement("li")
        li.innerText = photos[i].title
        let img = document.createElement("img")
        img.width = 50
        img.src = photos[i].thumbnailUrl
        li.insertAdjacentElement("beforeend",img)
        balisePhoto.insertAdjacentElement("beforeend",li)

        li.addEventListener("click", event => {
            for(let i=0; i<20; i++) {
                if(photos[i].title === event.target.innerText) {
                    let img = document.createElement("img")
                    img.src = photos[i].url
                    result.innerHTML = ""
                    result.insertAdjacentElement("beforeend",img)
                }
            }
        })

    }
}

// appelAPI()
appelAPIPlaceholderPhotos()
